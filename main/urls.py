from django.conf.urls import url
from django.urls import path
from .views import *

app_name = 'main'

urlpatterns = [
    path('', home, name='home'),
    path('story1',story1, name='story1'),
    path('lainnya',lainnya, name='lainnya'),
]
